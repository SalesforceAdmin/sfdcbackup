/*****************************************************************************************
Name: ReadersManagementDAO
Copyright © Infogain
==========================================================================================
==========================================================================================
Purpose: Controller to fetch queries from various objects related to Reader Management.
         Used by ReaderDetailsView, ReadersViewPaginationController, ReaderViewHelper classes.
==========================================================================================
==========================================================================================
History
-------
VERSION        AUTHOR                             DATE              DETAIL              
1.0            ApexChange- Readers Management     21-Mar-2016       Initial Development
               Rajeshwari & Krishna
******************************************************************************************/ 
public class ReadersManagementDAO {
         
    
    /******************************************************************************************
        Purpose     : To fetch Reader__c object details.
        Return      : String ReaderQuery
    ******************************************************************************************/
    public static string ReaderQuery(){
        String readerQuery  = 'SELECT Account__c,Active_Potential__c,Area_of_Specialization__c,';
        readerQuery         = readerQuery  + ' AssetID__c,Availability__c,Billing_Type_Reader_Contract__c, modality__c , MSA_GSA__c , Contract_Rate_per_Hour__c, Contract_TP_per_Hour__c, Projected_TP_Read_per_Hour__c, Calculated_Rate_per_Hour__c,';
        readerQuery         = readerQuery  + ' Billing_Type_Sponsor_Contract__c,BioClinica_Share__c,City__c,Close_Date__c,';
        readerQuery         = readerQuery  + ' Connection_Speed_Mbp_s__c,Connection_Type__c,Country__c,CreatedById,CreatedDate,CurrencyIsoCode,';
        readerQuery         = readerQuery  + ' Email_1__c,Email_2__c,First_Name__c,General_Experience__c,Id,IsDeleted,IT_Contact_on_Site__c, ';
        readerQuery         = readerQuery  + ' LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Last_Name__c,Medical_License_Expiration_Date__c,Contact__r.OtherPhone, ';
        readerQuery         = readerQuery  + ' MSA_GSA_Expiration_date__c,Name,Part_of_Share_Agreement_Y_N__c,Pass_Through_or_Direct_Service__c,Phone_1_Type__c,Phone_1__c,Contact__r.MobilePhone,';
        readerQuery         = readerQuery  + ' Phone_2_Type__c,Phone_2__c,Primary_PM__c,Reader_Affiliation_Group__c,Reader_Affiliation__c,Reader_Contract_Rate__c,Sponsor_Contract_Reader_Rate__c,Contact__r.Country_2__c,Contact__r.HomePhone,';
        readerQuery         = readerQuery  + ' State_Province__c,Street_Address_1__c,Street_Address_2__c,Study__c,SystemModstamp,Title__c,Visitation_History__c,Zip_Code__c,Contact__r.City_2__c,Contact__r.State_Province_2__c,Contact__r.Zip_Postal_Code_2__c,Contact__r.Fax,';
        readerQuery         = readerQuery  + ' Study__r.Name, Account__r.Name, Contact__r.Asset_ID__c, Contact__r.Alternate_Phone_Type__c,Contact__r.Phone_Type__c, Contact__r.Zip_Postal_Code__c, Contact__r.Street_Address_2__c, Contact__r.Street_Address_1__c, Contact__r.State__c, Contact__r.Country__c, Contact__r.City__c, Contact__r.Alternate_Email_2__c, Contact__r.Alternate_Email_1__c,Contact__r.Alternate_Email__c, Contact__r.Title, Contact__r.Email, Contact__r.MailingCountry, Contact__r.MailingPostalCode, Contact__r.MailingState, Contact__r.MailingCity, Contact__r.MailingStreet, Contact__r.OtherCountry, Contact__r.OtherPostalCode, Contact__r.OtherState, Contact__r.OtherCity, Contact__r.OtherStreet, Contact__r.FirstName, Contact__r.LastName, Contact__r.Id, Contact__c, Contact__r.phone,  Contact__r.Ext_ReaderID__c,  ';
        readerQuery         = readerQuery  + ' (Select Id, Name, Study__c From Reader_Study__r) , ';
        readerQuery         = readerQuery  +'  (Select Id, Name, Reader__c, Modality__c, Sponsor_Contract_Reader_Rate__c, Reader_Contract_Rate__c From Reader_Modality__r) ';
        readerQuery         = readerQuery  + ' FROM Reader__c';
        return readerQuery;
        
    }
    
    /******************************************************************************************
        Purpose     : To fetch the Study__c object details.
        Return      : String StudyQuery
    ******************************************************************************************/
    public static String StudyQuery(){
        String StudyQuery ='SELECT Account__c, id, Name , Account__r.Name from study__C ';
        return StudyQuery;
    }
    
    /******************************************************************************************
        Purpose     : To fetch the Reader_Study__c object details.
        Return      : String ReaderStudy
    ******************************************************************************************/
    public static String ReaderStudyQuery(){
        String ReaderStudy ='SELECT  id, Name, reader__C ,study__c, study__r.Name,study__r.Account__c,   study__r.Account__r.Name from Reader_Study__C ';
        return ReaderStudy;
    }
    
    /******************************************************************************************
        Purpose     : To fetch the Study_Team object details.
        Return      : String StudyTeamQuery
    ******************************************************************************************/
    public static String StudyTeamQuery(){
        String StudyTeamQuery ='Select primary__c, Role__c, Name, Study__c, Inactive__c, Id, Contact__c From Study_Team__c  ';
        return StudyTeamQuery;
        
    }
    /******************************************************************************************
        Purpose     : To fetch the Reader Contact object details.
        Return      : String ContactQuery
    ******************************************************************************************/
     public static String ContactQuery(){
        String ContactQuery ='Select id, firstname,lastname, Alternate_Email__c, Alternate_Email_1__c,City__c,Asset_ID__c,Street_Address_1__c,Street_Address_2__c,State__c,Primary_Contact__c,Phone_Type__c , Zip_Postal_Code__c From contact ' ;
        return ContactQuery;
        
    }
    
     /******************************************************************************************
        Purpose     : To fetch the ImagingFacility object details.
        Return      : String ContactQuery
    ******************************************************************************************/
     public static String ImagingFacilityQuery(){
        String ImgQuery ='Select id, modality__C, study__C, name   from Imaging_facility__c ' ;
        return ImgQuery;
        
    }
    
}