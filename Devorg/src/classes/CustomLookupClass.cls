public with sharing class CustomLookupClass 
{

    public String AccountNamePassToTextBox { get; set; }
    public String isAllSelectCheckBox { get; set; }
    public List<AccountWrapper> wrapAccountList {get; set;}

     public CustomLookupClass()
     {
        if(wrapAccountList == null) 
        {
            wrapAccountList = new List<AccountWrapper>();
            for(Account a: [select Id, Name from Account]) 
            {
                // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                wrapAccountList.add(new AccountWrapper(a));
            }
        }
     }
        
    public PageReference onCheck() 
    {
        AccountNamePassToTextBox = '';
        for(AccountWrapper aw : wrapAccountList)
        {
            if(aw.isChecked)
            {
               AccountNamePassToTextBox += aw.accname.name +';';
            }
        }
        
        return null;
    }

    public PageReference onSelectAll() 
    {
        return null;
    }
  
   
   
    
    /*Account Wrapper*/
    public class AccountWrapper
    {
         public Account accname {get; set;}
         public Boolean ischecked{get; set;}
 
        public AccountWrapper(Account a) 
        {
            accname = a;
       } 
    }    
}