public with sharing class currencyclass {

    public currencyclass(ApexPages.StandardController controller) 
    {

    }
    public String selectedIso {get;set;}

  public List<selectOption> isoCodes {
    get {
      List<selectOption> options = new List<selectOption>();

      for (ISO_Currencies__c iso : ISO_Currencies__c.getAll().values())
        options.add(new SelectOption(iso.ISO_Code__c,iso.ISO_Code__c+' - '+iso.Name));
      return options;

    }
    set;
  }


}