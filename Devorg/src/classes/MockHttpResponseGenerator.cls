@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global Httpresponse respond(HTTPRequest req) {
    req.getEndpoint();
    req.getMethod();
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://ip.jsontest.com/', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"ip": "181.60.88.8"}');
        res.setStatusCode(200);
        return res;
    }
}