@IsTest(seeAllData=False)
public class ReadersViewPaginationControllerTest{
   /*****************************************************************************************
        Purpose                 : Test method for Controller for ReadersViewPaginationController
                      exportAll, toggleSort, refresh and ClearSearch Method
        Cross Obejct            : Reader, Contact, Reader Study and Reader Modality
    ******************************************************************************************
   public static testmethod void ReadersViewPaginationControllerMethod(){
       Test.StartTest();  
       List<Reader_Study__C>testReaderstudy = ReaderManagementTestUtility.createTestReaderStudy();
     ReadersViewPaginationController reader = new ReadersViewPaginationController();  
     List<SelectOption> Specialiasation =  reader.getSpecialiasation();
     List<SelectOption> Modality =  reader.getModality();
     List<ReaderViewWrapper.ReaderDetails> readerList = reader.getReaders();
     reader.searchQuery = false;
     reader.exportAll();
     reader.toggleSort();
     reader.refresh();
     String xlsHeader  = reader.xlsHeader;
     reader.ReaderDetailpage();
     reader.osearchReader.Active_Potential__c ='Active';
     reader.ClearSearch();
       Test.StopTest();
   }
   
    /*****************************************************************************************
        Purpose                 : Test method for ContactDetailpage, ReaderDetails Method
        Cross Obejct            : Reader, Contact, Reader Study and Reader Modality
    ******************************************************************************************
   public static testmethod void ReadersViewMethod(){
       Test.StartTest();  
       List<Reader_Study__C>testReaderstudy = ReaderManagementTestUtility.createTestReaderStudy();
     ReadersViewPaginationController reader = new ReadersViewPaginationController();  
     List<SelectOption> Specialiasation =  reader.getSpecialiasation();
     List<SelectOption> Modality =  reader.getModality();
     Reader_Study__C rs = [select id, study__C, study__r.name , study__r.Account__C, study__r.Account__r.Name,  reader__C, reader__r.name, reader__r.Contact__C, 
                  reader__r.Contact__r.name from Reader_Study__C where study__r.name = '0001A' limit 1 ];
     system.assert(rs!=null);
     String StudyName = rs.study__r.name;
     Apexpages.currentPage().getParameters().put('StudyName',StudyName);
         String accountName = rs.study__r.Account__r.Name;
         Apexpages.currentPage().getParameters().put('accountName',accountName);
       reader.searchQuery();
       reader.exportAll();
       Apexpages.currentPage().getParameters().put('cid',rs.reader__r.Contact__C);
     reader.ContactDetailpage();
     Apexpages.currentPage().getParameters().put('rid', rs.reader__c);
     reader.ReaderDetails();
     //Exception Condition
     Apexpages.currentPage().getParameters().put('StudyName','qrt');
         Apexpages.currentPage().getParameters().put('accountName','actname');
       reader.searchQuery();
       Test.StopTest();
   }*/
 
}