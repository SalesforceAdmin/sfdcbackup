public with sharing class ReadersViewPaginationController {
    /*public Integer noOfRecords{get; set;}
    public Integer size{get;set;}
    public List<ReaderViewWrapper.ReaderDetails> oReaderDetails         {get;set;}
    public string recordid{get;set;}        
    public Reader__C osearchReader {get;set;}     
    public Boolean searchQuery = false;
    public String actPotential = null;
    public String ptDrService = null;
    public String StudyName = null;
    public String AccountName = null;
    //Added search functionality for modality field
    public String ModVal = null;
    public String aoSpecialisation = null;
    
    public string cid {get;set;}
    public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'Desc'; } return sortDir;  }
    set;
    }
      //Assing value to sortfield variable.
      public String sortField {
        get  { if (sortField == null) {sortField = 'Name'; } return sortField;  }
        set;
      }
     /******************************************************************************************
        Purpose     : Controller Method.
    ******************************************************************************************
    public ReadersViewPaginationController() {
        osearchReader = new Reader__C();
    }
    
    public List<SelectOption> getSpecialiasation(){
      List<SelectOption> options = new List<SelectOption>();
       Schema.DescribeFieldResult fieldResult = Reader__C.Area_of_Specialization__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add(new SelectOption('--None--', '--None--'));
       for( Schema.PicklistEntry f : ple)
       {
          options.add(new SelectOption(f.getLabel(), f.getValue()));
       }       
       return options;
    }
    
     public List<SelectOption> getModality(){
      List<SelectOption> options = new List<SelectOption>();
       Schema.DescribeFieldResult fieldResult = Reader__C.Modality__C.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       options.add(new SelectOption('--None--', '--None--'));
       for( Schema.PicklistEntry f : ple)
       {
          options.add(new SelectOption(f.getLabel(), f.getValue()));
       }       
       return options;
    }
    
    /******************************************************************************************
        Purpose     : To set page size, fetch count of total records
    ******************************************************************************************
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                Boolean whereflag = false;
                size = 20;
                //Fetching readerQuery and assigning to string variable.
                string queryString = ReadersManagementDAO.ReaderQuery();
                if(searchQuery){
                    set<String>readerid = new set<String>();
                     //Fetching readerID by passing accoutname and studyname and assigning to variable.
                     readerid =ReaderViewHelper.getReaderStudyDetail(AccountName,  StudyName);
                     //checking if readerid is not empty.                    
                     if(!readerid.isempty()){
                        //Adding condition to querystring.
                        queryString += ' where id in : readerid';
                        //Assigning value to whereflag variable.
                        whereflag = true;         
                     }else{
                        //Checking if accountname, studyname is not null or empty.
                        if((AccountName !=null && AccountName !='') ||(StudyName!=null && StudyName!='')){
                            //Adding condition to querystring.
                             queryString += ' where name = null ';
                             //Assigning value to whereflag variable.
                             whereflag = true;
                        }
                     }
                     //Fetching Readerquery and assigning to string variable.
                     queryString  =  ReaderViewHelper.getReaderQuery (queryString, whereflag ,actPotential,ptDrService, aoSpecialisation, ModVal);
                 }
                //Adding condition to querystring.
                queryString += ' order by '+sortField+' '+sortDir;
                //Fetching records of Reader object.
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                //Setting page size.
                setCon.setPageSize(size);
                //Assigning count of total records.
                noOfRecords = setCon.getResultSize();
               
            }
            return setCon;
        }set;
    }
    /******************************************************************************************
        Purpose     : To fetch list of Reader__c object records.
        Return      : accList
    ******************************************************************************************
    Public List<ReaderViewWrapper.ReaderDetails> getReaders(){
        List<ReaderViewWrapper.ReaderDetails> accList = new List<ReaderViewWrapper.ReaderDetails>();
        List<Reader__c> oReaderList = new List<Reader__c> ();
        //Checking if setcon is not null.
        if(setCon !=null){
            oReaderList = (List<Reader__c>)setCon.getRecords();  
            //Checking if search is performed or not.         
            if(!searchQuery){
                accList =  ReaderViewHelper.AssignReaderDetailstoWrppaer(oReaderList,null, null);
            }else{
                accList =  ReaderViewHelper.AssignReaderDetailstoWrppaer(oReaderList,accountName, StudyName);
            }
        }
        else{
             //Display error message if no records are found.
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ' No results found'));   
             return null;
        }
        return accList;
    }
    
    /******************************************************************************************
        Purpose     : To check if sort is ascending or descending.
    ******************************************************************************************
    public void toggleSort() {
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
        setCon = null;
        //Calling getReaders method.       
        getReaders();
    }
    /******************************************************************************************
        Purpose     : To get list of Reader__c object records for export functionality.
        Return      : oReaderDetails list.
    ******************************************************************************************
    Public List<ReaderViewWrapper.ReaderDetails> getExportReaders(){
        List<ReaderViewWrapper.ReaderDetails> accList = new List<ReaderViewWrapper.ReaderDetails>();
       oReaderDetails = new  List<ReaderViewWrapper.ReaderDetails>();        
       oReaderDetails = ReaderViewHelper.getReaderDetailsExport(searchquery,accountName, StudyName, actPotential, ptDrService , aoSpecialisation, ModVal );
        return oReaderDetails;
    }
    
    /******************************************************************************************
        Purpose     : Filter the results by using Study code (or) Account Name (or) active potential
                      (or) Modality (or) Specialization (or) PassthroughDirectService.
    ******************************************************************************************
    public PageReference SearchQuery() {
        oReaderDetails = new List<ReaderViewWrapper.ReaderDetails> ();
        searchQuery = true;
        //Fetch studyname and accountname from url.
        StudyName = Apexpages.currentPage().getParameters().get('StudyName');
        accountName = Apexpages.currentPage().getParameters().get('accountName');
        //Assigning field values of Active_Potential__c and Pass_Through_or_Direct_Service__c to string variables.
        actPotential = osearchReader.Active_Potential__c;
        ptDrService  = osearchReader.Pass_Through_or_Direct_Service__c;
        ModVal = osearchReader.Modality__C;
        aoSpecialisation = osearchReader.Area_of_Specialization__c;
        setCon = null;
         //calling getReaders() method.
         getReaders();
        return null;
    }
    /******************************************************************************************
        Purpose     : Refresh the view.
    ******************************************************************************************
    public pageReference refresh() {
        setCon = null;
        getReaders();
        setCon.setPageNumber(1);
        return null;
    }
        
    /******************************************************************************************
        Purpose     : Method to navigate to the ReaderDetailsViewPage
                      based on selected Reader record Id.
        Return      : ReaderDetailsView URL with record id.
    ******************************************************************************************
    public PageReference ReaderDetails(){
        recordid = Apexpages.currentPage().getParameters().get('rid');
        PageReference pg = new PageReference('/apex/ReaderDetailsView?id='+recordid);
        pg.setredirect(true);
        return pg; 
    }
    
    /******************************************************************************************
        Purpose     : xml schema for displaying excel file.
        Return      : string strHeader
    ******************************************************************************************
    public String xlsHeader {
        get {
            String strHeader = '';
            strHeader += '<?xml version="1.0"?>';
            strHeader += '<?mso-application progid="Excel.Sheet"?>';
            return strHeader;
        }
    }
     
    /******************************************************************************************
        Purpose     : Method which navigates to ExportReaderRecords page
                      which contains all field values of Reader object in excel format.
        Return      : ExportReaderRecords URL
    ******************************************************************************************
    public Pagereference exportAll(){
        getExportReaders();
        return new Pagereference('/apex/ExportReaderRecords');
    }
    
    /******************************************************************************************
        Purpose     : Method which clears the search field values.
    ****************************************************************************************** 
    public Pagereference clearSearch(){
        if(osearchReader.Active_Potential__c != '' || osearchReader.Pass_Through_or_Direct_Service__c != '' || osearchReader.Modality__C !='' ||osearchReader.Area_of_Specialization__c !=''){
            osearchReader.Active_Potential__c='';
            osearchReader.Pass_Through_or_Direct_Service__c='';
            osearchReader.Area_of_Specialization__c = '';
            osearchReader.Modality__C ='';
           return SearchQuery(); 
        } 
        return null;
    }
    /******************************************************************************************
        Purpose     : Navigate to ReadersEditPage to edit Reader__c object records.
        Return      : ReadersEditPage URL
    ******************************************************************************************
    public PageReference ReaderDetailpage(){
        PageReference pg = new PageReference('/apex/ReadersEditPage');
        pg.setredirect(true);
        return pg;
    } 
    /******************************************************************************************
        Purpose     : Navigate to Contact Detail page.
        Return      : Contact page.
    ******************************************************************************************
    public PageReference ContactDetailpage(){
        cid = Apexpages.currentPage().getParameters().get('cid');
        PageReference pg = new PageReference('/'+cid);
        pg.setredirect(true);
        return pg;
    } */
}