public class nestedqueryexample
{
  set<id> accountids = new set<id>();
  list<account> accounts = new list<account>();
  list<contact> conrecords = new list<contact>();
  public List<Account> getaccsandtmember()
  {
      accounts = [Select Id,(select name from account.contacts), Name, BillingCountry from Account];
      return accounts;
  }
  public list<contact> getcontacts()
  {
      for(Account acc : accounts)
      {
          accountids.add(acc.id);
      }
      for(Contact con : [select Name from contact where accountid=:accountids])
      {
          conrecords.add(con);
      }
      return conrecords;
  }
}