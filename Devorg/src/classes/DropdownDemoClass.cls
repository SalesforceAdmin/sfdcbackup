public with sharing class DropdownDemoClass 
{
    public string SelectedAccount{get;set;}
    public string SelectedStudy{get;set;}
    public DropdownDemoClass()
    {
        getAccounts();
        getStudies();
    }
    
    public List<SelectOption> getAccounts()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        if(SelectedAccount == null)
        {
            options.add(new SelectOption('--None--','--None--'));
            for(Account acc:[select id,name from account])
            {
                
                options.add(new SelectOption(acc.name,acc.name));
            }
        }
        
       
           if(SelectedStudy != null)
           {
                study__c querystudy = [select id,name,account__c from study__c where name=:selectedstudy];              
                for(Account acc:[select id,name from account where id =: querystudy.account__c ])
                {
                  options.add(new SelectOption(acc.name,acc.name));
                }
           }    
        return options;
    }
    
     public List<SelectOption> getStudies()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        if(SelectedStudy == null)
        {
            options.add(new SelectOption('--None--','--None--'));
            for(Study__c st:[select id,name from study__c])
            {
                
                options.add(new SelectOption(st.name,st.name));
            }
        }   
       
        if(SelectedAccount != null)
        {           
            options.add(new SelectOption('--None--','--None--'));
            for(Study__c s:[select id,name from study__c where account__r.name=:SelectedAccount])
            {
                options.add(new SelectOption(s.name,s.name));
            }
        }
          
        return options;
    }
    
   public pagereference Reload()
   {
       pagereference ref = new pagereference('/apex/DropdownDemo');
       ref.SetRedirect(true);
       return ref;
   }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

}