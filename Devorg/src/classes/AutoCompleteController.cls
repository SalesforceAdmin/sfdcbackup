public with sharing class AutoCompleteController {

    public String selectedAccount { get; set; }
    public String searchTerm { get; set; }
    
    // JS Remoting action called when searching for a account name
    @RemoteAction
    public static List<Account> searchAccount(String searchTerm) {
        System.debug('Account Name is: '+searchTerm );
        List<Account> accounts = Database.query('Select Id, Name from Account where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'');
        return accounts;
}
}