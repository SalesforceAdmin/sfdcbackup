public with sharing class Attachtopdf 
{
    public account acc{get;set;}
    public account pid{get;set;}
    public string doSave{get;set;}
    
    public Attachtopdf(ApexPages.StandardController controller) 
    {
        pid = (Account)controller.getrecord(); 
        doSave = ApexPages.currentPage().getParameters().get('doSave');
        try
        {
           acc = [Select id, Name From Account where ID =: pid.id]; 
           system.debug(acc.id);
        }
        catch (Exception e) 
        {
            // add your message to the user or
            acc = new Account();
        }     
    }
    
    public pagereference attachpdf()
    {
        if(doSave == 'No')
        { return null; }
        if(acc.id != null)
        {
            PageReference pagePdf = page.pdfview; 
            pagePdf.getParameters().put('id', acc.id);  
            pagePdf.getParameters().put('doSave', 'No');           
            Blob pdfPageBlob; 
            pdfPageBlob = pagePdf.getContentaspdf();
             Attachment a = new Attachment(); 
             a.Body = pdfPageBlob;
             a.ParentID = acc.id; 
             system.debug('value'+a.parentid);
             a.Name = acc.name+'.pdf'; 
             a.Description = 'TestDescription1';
             insert a;
             system.debug('list---'+a);
        }
        
         pagereference pref = new apexpages.standardcontroller(pid).view();
         pref.setredirect(true);
         return pref;
    }

}