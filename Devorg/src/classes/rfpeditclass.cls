public with sharing class rfpeditclass 
{
    apexpages.standardcontroller controller{get;set;}
    public rpf__c rfp{get;set;}
    public id rfpid{get;set;}
    
    public account acc{get;set;}
    
    public rfpeditclass(ApexPages.StandardController controller) 
    {
        this.controller = controller;
        this.rfp = (rpf__c)controller.getrecord();
    }
    
    public Pagereference saverfpdetails()
    {
        PageReference ref = controller.save();
        rfpid = controller.getId();
        ref =  new PageReference('/apex/Rfpdetailpage?id='+rfpid);
        ref.SetRedirect(true);
        return ref;
    }
    
    public void populateaccountfields()
    {
        rfp.accountname__r = [select accountstatus__c,accountnumber__c from account where id =: rfp.accountname__c limit 1];
    }
    
    
    

}