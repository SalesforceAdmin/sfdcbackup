public class Httpcalloutclass
{
    
public static HttpResponse getCalloutResponseContents(string output) 
  { 
    // Instantiate a new http object
    Http h = new Http();

     // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
    HttpRequest req = new HttpRequest();
    req.setEndpoint(' http://ip.jsontest.com/');
    req.setMethod('GET');
    
    // Send the request, and return a response
    HttpResponse res = h.send(req);
    System.debug(res.toString());
    System.debug('STATUS:'+res.getStatus());
    System.debug('STATUS_CODE:'+res.getStatusCode());
    System.debug('Content: ' + res.getBody());
    JSONParser parser = JSON.createParser(res.getBody());
               parser.nextToken();
               parser.nextValue();
    string fieldvalue = parser.getText();       
    system.debug('*****JSON'+fieldvalue);
    return res;
   }
    
}