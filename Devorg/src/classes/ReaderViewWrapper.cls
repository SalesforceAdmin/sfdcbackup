/*****************************************************************************************
Name: ReaderViewWrapper
Copyright © Infogain
==========================================================================================
==========================================================================================
Purpose: Controller associated with classes which has property variables of Reader__c object.
         Used by ReaderDetailsView, ReadersViewController, ReaderViewHelper classes.
==========================================================================================
==========================================================================================
History
-------
VERSION        AUTHOR                             DATE              DETAIL              
1.0            ApexChange- Readers Management     21-Mar-2016       Initial Development
               Rajeshwari
******************************************************************************************/ 
public  class ReaderViewWrapper {
    
    /******************************************************************************************
        Purpose     : Contains property variables of Reader__c object fields.
    ******************************************************************************************
    public class ReaderDetails {
             
        public String address           {get;set;}
        public String phone             {get;set;}
        public String email             {get;set;} 
        public String modality          {get;set;}
               
        //Information section fields
        public String readerName                        {get;set;} 
        public String recordId                          {get;set;}
        public String readerId                          {get;set;}
        public String contactId                         {get;set;}
        public String FirstName                         {get;set;}
        public String LastName                          {get;set;}
        public String StreetAddress1                    {get;set;}
        public String StreetAddress2                    {get;set;}
        public String City                              {get;set;}
        public String City2                             {get;set;}
        public String StateorProvince2                  {get;set;}
        public String StateorProvince                   {get;set;}
        public String ZipCode                           {get;set;}
        public String ZipCode2                          {get;set;}
        public string Country                           {get;set;}
        public string OtherPhone                        {get;set;}
        public string Country2                          {get;set;}
        public string otherEmail                        {get;set;}
        public string mobile                            {get;set;}
        public string homePhone                         {get;set;}              
        public String Title                             {get;set;}
        public List<SelectOption> PrimaryPhoneType      {get;set;}
        public String PrimaryPhone                      {get;set;}
        public List<SelectOption> SecondaryPhoneType    {get;set;}
        public String SecondaryPhone                    {get;set;}
        public String fax                               {get;set;}
        public Integer extReaderId                               {get;set;}
        public String PrimaryEmail                      {get;set;}
        public String AlternateEmail                    {get;set;}
        
        //Study Information
        public String accountName                       {get;set;}
        public String studyCode                         {get;set;}
        public String PrimaryPM                         {get;set;}
        public Date CloseDate                           {get;set;}
        public Boolean StudyExist                          {get;set;}
        
          //Experience
          public string ReaderAffiliation                       {get;set;}
          public string ReaderAffiliationGroup                  {get;set;}
          public Date MedicalLicenseExpirationDate                          {get;set;}
          public string AreaofSpecialization                    {get;set;}
          public String Availability                                        {get;set;}
          public string GeneralExperience                       {get;set;}
          public List<SelectOption> ActiveStudies                           {get;set;}
          public string ActivePotential                         {get;set;}
          public String VisitationHistory                                   {get;set;}
          public String Pass_Through_or_Direct_Service                                  {get;set;}
                  
          //Contract Agreement
          public Decimal BioclinicaShare                                    {get;set;}
          public List<SelectOption> MSAGSA                                  {get;set;}
          public Date MSAGSAExpirationdate                                  {get;set;}
          public List<SelectOption> PartofShareAgreement                    {get;set;}
          public String ReaderContractRate                                  {get;set;}
          public String SponsorContractReaderRate                           {get;set;}
          public List<SelectOption> BillingTypeReaderContract               {get;set;}
          public List<SelectOption> BillingTypeSponsorContract              {get;set;}
          public double Curency                                             {get;set;}
          public List<SelectOption> PassThroughorDirectService              {get;set;}
          public String CalculatedRateperHour                               {get;set;}
          public String ContractRateperHour                                 {get;set;}
          public String ContractTPperHour                                   {get;set;}
          public String ProjectedTPReadperHour                              {get;set;}
          
          //Site Information         
          public String ITContactonSite                                     {get;set;}
          public String ConnectionType                                      {get;set;}
          public String ConnectionSpeedMbp                                  {get;set;}
          public String Asset                                               {get;set;}         
          public List<Studydetails> oStudydetails                           {get;set;}
    }
    
    //StudyDetails Information
    public class Studydetails {     
        public String accountName                       {get;set;}
        public String studyCode                         {get;set;}
        public String AccountID                         {get;set;}
        public String StudyID                           {get;set;}       
    }*/
}