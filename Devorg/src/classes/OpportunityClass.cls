public with sharing class OpportunityClass {

    public list<OpportunityWrapper> Oppwrplist {set;get;}
    
    public OpportunityClass(ApexPages.StandardController controller) 
    {
        GetOpportunityRecords();
    }
    
    //--- Code to display records of opportunity starts here ---//
    public list<OpportunityWrapper> GetOpportunityRecords()
    {
        list<opportunity> opplist = [select name,totalopportunityquantity,amount from opportunity limit 5];
        Oppwrplist = new list<OpportunityWrapper>();
        for(opportunity o : opplist)
        {
            Oppwrplist.add(new OpportunityWrapper(o,false));
        }
        
        return Oppwrplist;
    }
    //--- Code to display records of opportunity ends here ---//
    
    //--- Code to validate selected records starts here ---//
    public pagereference ValidateRecords()
    {
        for(OpportunityWrapper oppr : Oppwrplist)
        {
            
            /*if(chkselected == true)
            {
                if(Oppwrplist.size() > 1)
                {
                    apexpages.addmessage(new apexpages.Message(apexpages.severity.error,'Only one record must be selected'));
                }
            }*/
        }
        return null;
    }
    //--- Code to validate selected records ends here ---//    
    
    //---- Code for wrapper class starts here ---//
    public class OpportunityWrapper
    {
        public Opportunity op{set;get;}
        public boolean chkselected{set;get;}
        
        public OpportunityWrapper(Opportunity opt, boolean chkbox)
        {
            op          = opt;
            chkselected = chkbox;
        }
    }
    
    //--- Code for wrapper class ends here ---//

}