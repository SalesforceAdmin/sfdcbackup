/***********************************************************************************************************************************************
Name: ReaderStudy_TriggerHandler
Copyright © Infogain
================================================================================================================================================
================================================================================================================================================
Purpose: Helper Class for Readerstudy_trigger
================================================================================================================================================
================================================================================================================================================
History
-------
VERSION        AUTHOR                             DATE              DETAIL              
1.0            Rajeshwari Padmasali               21-Mar-2016       check duplicatestudy and assignprimaryPM.
                                                                    Used in Readerstudy_trigger and ReaderstudyController class.
************************************************************************************************************************************************/
/*****************************************************************************************
Name: ReaderStudy_Trigger
Copyright © Infogain
==========================================================================================
==========================================================================================
Purpose: Trigger on Reader_Study__c Object
==========================================================================================
==========================================================================================
History
-------
VERSION        AUTHOR                             DATE              DETAIL              
1.1            Rajeshwari Padmasali               23-Mar-2016      Initial development 

******************************************************************************************/ 
/*trigger ReaderStudy_Trigger on Reader_Study__c (before insert, before update) {
    System.debug('ReaderStudy_Trigger .starts here -->');
    if(!Global_ApexStaticController.Reader_Study_Trigger_Off){
      Global_ApexStaticController.Reader_Study_Trigger_Off = true;  
      if(Trigger.isbefore){
          // AFTER INSERT
          if(Trigger.isInsert){
              ReaderStudy_TriggerHandler.rs_Insert(Trigger.new, 'Insert'); 
          }
      }
      Global_ApexStaticController.Reader_Study_Trigger_Off = false;
    }
    System.debug('ReaderStudy_Trigger .Ends here -->');
}*/
public class ReaderStudy_TriggerHandler {
    /******************************************************************************************
        Purpose     : Helper method for Insert Reader Study Trigger.
        Parameters  : readerStudyList,Trigger State(After, Before)
        Returns     : Assign Primary PM 
    ******************************************************************************************/
    /*public static void rs_Insert(List<Reader_Study__c> readerStudyList, String mode) {      
        set<Id> oStudyID =new set<Id>();
        set<Id>  readerMap = new set<Id> ();
        for(Reader_Study__c rd: readerStudyList){
            if(rd.study__C !=null){
                oStudyID.add( rd.study__C);
                readerMap.add(rd.reader__c);
            }
        }       
        if(!readerMap.isempty()){
            if(!oStudyID.isempty() ){
                //Assigns Primary PM wrt Study 
                AssignPrimaryPM(oStudyID, readerStudyList);
            }
        }
    }*/
    
    /******************************************************************************************
        Purpose     : Method to check if duplicate study__c for Reader.
        Parameters  : readerMap,readerStudyList
        Return      : value of DuplicateFlag. TRUE (Reader Exist) FALSE (No Reader)
    ******************************************************************************************/
    /*public static Boolean checkDuplicateStudy(set<Id>  readerMap , List<Reader_Study__c> readerStudyList){
        Boolean DuplicateFlag = false;
        try{
            //Fetching ReaderStudyQuery.
            String ReaderStudyQuery = ReadersManagementDAO.ReaderStudyQuery();
            set<string> readerStudy = new set<String>();
            //Quet wrt ReaderID
            ReaderStudyQuery += ' where reader__C in : readerMap';
            List<Reader_Study__c> rsList = database.query(ReaderStudyQuery);
            //Looping on Reader_Study__c object and Assign to readerStudy set  "ReaderID-StudyID".
            for(Reader_Study__c rs : rsList){               
                readerStudy.add(rs.reader__c+'-'+rs.study__C);
            }           
            //Looping on Reader_Study__c object to check duplicate against readerStudy set .
            for(Reader_Study__c rd : readerStudyList){
                String rs = rd.reader__c+'-'+rd.study__C; 
                //To check if duplicate study exists and setting duplicateflag to true.              
                if(!DuplicateFlag){
                    //To check collection readerstudy contains duplicate study id's and assign value to DuplicateFlag variable.
                    if(readerStudy.contains(rs)){
                        DuplicateFlag = true;
                    }
                }
            }
        }
        catch(Exception ex){                       
        }
       
        return DuplicateFlag;
    }*/
    /******************************************************************************************
        Purpose     : Method to assign Primary PM  while Linking study to Reader.
        Parameters  : OStudydID, ReaderStudyList
        Return      : Primary PM to Reader Study
    ******************************************************************************************/ 
   /* public static void AssignPrimaryPM(set<Id> oStudyID, List<Reader_Study__c> readerStudyList){
        
        //Query to fetch details form Study_Team__c object. wrt to study.
        String studyteamQuery = ReadersManagementDAO.StudyTeamQuery();
        String role = 'CPM - Clinical Project Manager';     
        StudyTeamQuery += ' where  study__C in :oStudyID and Inactive__c = false and primary__c =true and role__C = :role order by createdDate desc limit 1';
        List<Study_team__C> oStudyTeam = database.query(StudyTeamQuery);
        List<Reader_Study__c> ostlist = new List<Reader_Study__c>();
        map<Id, Id> oStudyteamID =new map<Id, Id>();
        //Looping on Study_Team__c object.
        for(Study_team__C st : oStudyTeam){
            //Assigning key(StudyID), values to Study Team Id to oStudyteamID Map.
            oStudyteamID.put(st.study__C , st.id); 
        }
        //Looping on Reader_Study__c List.
        for(Reader_Study__c rd: readerStudyList){
            //Checking if Map oStudyteamID contains values.
            if(oStudyteamID.containsKey(rd.study__C )){
                //Assigning value to study_Team__c field of Reader Study Record.
                rd.study_Team__C = oStudyteamID.get(rd.study__C );              
            }
        }       
    }*/
}