public class ReaderDetailsViewController {
    // creating variable and instance for Reader object
    /*public String Rid                                       {get;set;}
    public Reader__C oReader                                {get;set;}
    public String ContactID                                 {get;set;}
    public List<Reader_Modality__c> oReaderModality         {get;set;}
    public boolean modality                                 {get;set;}
    public string tabID                                     {get;set;}
    /******************************************************************************************
        Purpose     : Standard controller method to fetch contact and reader object id's from url.
    ******************************************************************************************
     public ReaderDetailsViewController(ApexPages.StandardController stdController) {      
        oReader = new Reader__C();
        oReaderModality  = new List<Reader_Modality__c>();
        modality = false;
        Tab_ID__c tid = Tab_ID__c.getValues('Reader');
        tabID = tid.Id__c;
        rid = ApexPages.currentPage().getParameters().get('id');              
        ContactID = ApexPages.currentPage().getParameters().get('Conid');
        //Checking if contact id is not null.
        if(ContactID !=null && ContactID !=''){
            oReader.Contact__c = ContactID;
        }      
        //Checking if ReaderId is not null.
        if(rid !=null ){           
            //Fetching reader query from ReadersManagementDAO class.
            String readerQuery = ReadersManagementDAO.ReaderQuery();
            readerQuery +=  ' where id=:rid ';
            oReader = database.query(readerQuery);  
            getModality();
         }     
     }
     /******************************************************************************************
        Purpose     : Method to retrive all modality value 
        Parameters  : oReader Modality details
        Returns     : duplicate Exist  ? Error Message else Insert/Update Reader Study
    ******************************************************************************************
     public void getModality(){
        oReaderModality  = new List<Reader_Modality__c>();
        oReaderModality = ReaderViewHelper.ModalityViewList( oReader , false);
        if(!oReaderModality.isempty()){
            Modality = true;
        }else{Modality = false; }
     }
     /******************************************************************************************
        Purpose     : Method to save Reader__c object record and navigate back to the
                      ReadersView Page. 
        Parameters  : oReader  details
        Returns     : Redirect to ReadersView Page. 
    ******************************************************************************************
     public pagereference SaveReader (){
        //Checking if contact__c field in Reader__c object is equal to null
        if(oReader.Contact__C == null ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, ' Contact: You must enter a value'));return null;
        }
        //Fetch result from checkDuplicateReader method.
        Boolean dupReader = ReaderViewHelper.checkDuplicateReader(oReader);
        //Check if dupReader variable is true or false.
        if(!dupReader){
            try{
                upsert oReader; 
                ReaderViewHelper.UpsertReaderModality(oReaderModality,  oReader);
            }
            catch(Exception ex){
            }
            
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Unable to save. The selected Contact is already assigned as a Reader.'));return null;
        }
        //Redirect to Readersview page.
        pagereference pg = new pagereference('/apex/ReaderDetailsView?id='+oReader.ID+'&sfdc.tabName='+tabID);
        pg.SetRedirect(true);
        return pg;     
     }
    /******************************************************************************************
        Purpose     : Method to delete Reader__c object record and navigate back to the
                      ReadersView Page. 
        Parameters  : oReader  details
        Returns     : Redirect to ReadersView Page. 
    ******************************************************************************************
    public pagereference DeleteReader (){ 
        String Cid = oReader.Contact__C;   
        //Delete Reader__c object record.
        try{
            delete oReader; 
        }
        catch(Exception ex){
        }
        pagereference pg = new pagereference('/apex/ReadersView?sfdc.tabName='+tabID);
        //Checking if variable Cid is not null and readerid is not null.
        if(Cid !=null && rid ==null){
             pg = new pagereference('/'+Cid);
        }
        pg.setRedirect(true);
        return pg;      
     }
    /******************************************************************************************
        Purpose     : Method to navigate back to the ReadersView Page
        Parameters  : tabID and ContactID  details
        Returns     : Redirect to ReadersView Page. 
    ******************************************************************************************
      public pagereference CancelReader(){
        pagereference pg = new pagereference('/apex/ReadersView?sfdc.tabName='+tabID);
        //Checking if contact id is not null.
        if(ContactID !=null){
             pg = new pagereference('/'+ContactID);
        }
        pg.setRedirect(true);
        return pg;      
     } */ 
}