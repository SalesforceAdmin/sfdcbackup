@IsTest(seeAllData=false)
private class ReaderViewHelperTest{
  /*****************************************************************************************
        Purpose                 : Test method for ReadersViewPaginationController helper Method
        Cross Obejct            : Reader, Contact, Reader Study and Reader Modality
    ******************************************************************************************
  public static testmethod void HelperMethod_ReadersViewPaginationController(){
    Test.StartTest();  
       List<Reader_Study__C>testReaderstudy = ReaderManagementTestUtility.createTestReaderStudy();
     List<Reader_Study__C> rsList = [select id, study__C, study__r.name , study__r.Account__C, study__r.Account__r.Name,  reader__C, reader__r.name,
                       reader__r.Contact__C, reader__r.Contact__r.Firstname from Reader_Study__C ];
     system.assert(rsList.size() >0 );
     Map<String, Reader_Study__C> rmStudyNmToIdMap = new Map<String, Reader_Study__C>();
         for(Reader_Study__C s : rsList){
            rmStudyNmToIdMap.put(s.reader__r.Contact__r.firstname, s);
         }
     Reader_Study__C rs = rmStudyNmToIdMap.get('Reader1');
     String StudyName = rs.study__r.name;
     Apexpages.currentPage().getParameters().put('StudyName',StudyName);
         String accountName = rs.study__r.Account__r.Name;
         Apexpages.currentPage().getParameters().put('accountName',accountName);
         ReadersViewPaginationController reader = new ReadersViewPaginationController();  
     List<SelectOption> Specialiasation =  reader.getSpecialiasation();
     List<SelectOption> Modality =  reader.getModality();
     List<ReaderViewWrapper.ReaderDetails> readerList = reader.getReaders();
     reader.searchQuery = false;
     reader.exportAll();
       reader.searchQuery();
       reader.exportAll();
       
       Apexpages.currentPage().getParameters().put('StudyName','test@@@');
         Apexpages.currentPage().getParameters().put('accountName','');
       reader.searchQuery();
       reader.exportAll();
       
       Test.StopTest();
  }
  /*****************************************************************************************
        Purpose                 : Test method for getReaderQuery
        Cross Obejct            : Reader, Contact, Reader Study and Reader Modality
    ******************************************************************************************
  public static testmethod void HelperMethod_getReaderQuery(){
    Test.StartTest();  
       List<Reader_Study__C>testReaderstudy = ReaderManagementTestUtility.createTestReaderStudy();
     List<Reader_Study__C> rsList = [select id, study__C, study__r.name , study__r.Account__C, study__r.Account__r.Name,  reader__C, reader__r.name,
                       reader__r.Contact__C, reader__r.Contact__r.Firstname from Reader_Study__C ];
     system.assert(rsList.size() >0 );
     Map<String, Reader_Study__C> rmStudyNmToIdMap = new Map<String, Reader_Study__C>();
         for(Reader_Study__C s : rsList){
            rmStudyNmToIdMap.put(s.reader__r.Contact__r.firstname, s);
         }
     Reader_Study__C rs = rmStudyNmToIdMap.get('Reader1');
     String StudyName = rs.study__r.name;
     Apexpages.currentPage().getParameters().put('StudyName',StudyName);
         String accountName = rs.study__r.Account__r.Name;
         Apexpages.currentPage().getParameters().put('accountName',accountName);
         
         ReadersViewPaginationController reader = new ReadersViewPaginationController();  
     List<SelectOption> Specialiasation =  reader.getSpecialiasation();
     List<SelectOption> Modality =  reader.getModality();
         reader.osearchReader.Active_Potential__c ='test';
         reader.osearchReader.Pass_Through_or_Direct_Service__c='test';
         reader.osearchReader.Modality__C='test';
         reader.osearchReader.Area_of_Specialization__c='test';
       reader.searchQuery();
       reader.exportAll();
       
       Apexpages.currentPage().getParameters().put('StudyName','');
         Apexpages.currentPage().getParameters().put('accountName','');
         reader.osearchReader.Active_Potential__c ='test';
         reader.osearchReader.Pass_Through_or_Direct_Service__c='test';
         reader.osearchReader.Modality__C='test';
         reader.osearchReader.Area_of_Specialization__c='test';
       reader.searchQuery();
       
         reader.osearchReader.Active_Potential__c ='';
         reader.osearchReader.Pass_Through_or_Direct_Service__c='test';
         reader.osearchReader.Modality__C='test';
         reader.osearchReader.Area_of_Specialization__c='test';
       reader.searchQuery();
      
         reader.osearchReader.Area_of_Specialization__c='';
         reader.osearchReader.Modality__C='test';
       reader.searchQuery();
       
         reader.osearchReader.Modality__C= null;
         reader.osearchReader.Pass_Through_or_Direct_Service__c='';
       reader.searchQuery();
       
       Test.StopTest();
  }
  /*****************************************************************************************
        Purpose                 : Test method for ReaderDetailsViewController class
        Cross Obejct            : Reader, Contact, Reader Study and Reader Modality
    ******************************************************************************************
  public Static testmethod void HelperMethod_ReaderDetailsViewController(){
    Test.StartTest();  
    ReaderManagementTestUtility.createTestTab(); 
      List<Reader_Study__C>testReaderstudy = ReaderManagementTestUtility.createTestReaderStudy();
      List<Reader__C> testReaders = [select id, name ,  Contact__C, Contact__r.firstname from Reader__C];
      Map<String, Reader__c> rmodalityNmToIdMap = new Map<String, Reader__c>();
        for(Reader__c s : testReaders){
            rmodalityNmToIdMap.put(s.Contact__r.firstname, s);
        }
        Reader__c oReaderobj = rmodalityNmToIdMap.get('Reader1');
      ReaderManagementTestUtility.createTestReaderModality();
      ApexPages.currentPage().getParameters().put('id', oReaderobj.id);
      ApexPages.currentPage().getParameters().put('Conid',oReaderobj.Contact__C);
    ApexPages.StandardController stdController = new ApexPages.StandardController(oReaderobj);
        ReaderDetailsViewController controller = new ReaderDetailsViewController(stdController);
        controller.SaveReader();
        controller.oReader = oReaderobj;
        controller.oReader.Modality__C ='CT;MRI;DXA;XRAY';
        controller.getModality();
        controller.SaveReader();
      Test.stopTest();
  }*/
}