public class ReaderStudyController {
     //Property variables
    /* public boolean error                       {get;set;}
     public string readerID                     {get;set;}
     //public string ReaderStudyID                {get;set;}
     public string ReaderName                   {get;set;}
     public Reader_Study__c oReaderStudy        {get;set;}
     public Boolean EditFlag                    {get;set;}
     public Reader__c ReaderRecord              {get;set;}
     
    /******************************************************************************************
        Purpose     : Standard Controller Method to fetch reader Record 
    ******************************************************************************************
     public ReaderStudyController(ApexPages.StandardController stdController) {           
        oReaderStudy = new Reader_Study__c();
        ReaderRecord = new Reader__c();
        error = false;        
        EditFlag = false;
        readerID = ApexPages.currentPage().getParameters().get('readerID'); 
        if(readerId !=null && readerID != ''){
            oReaderStudy.Reader__C  = readerID;
            //Fetching Reader__c object record.
            ReaderRecord = [select contact__r.FirstName,contact__r.lastname from Reader__c where id = :readerID limit 1];
            //Assigning FirstName and LastName to sring variable.
            ReaderName = ReaderRecord.contact__r.FirstName+' '+ReaderRecord.contact__r.LastName;
        }
     }
    /******************************************************************************************
        Purpose     : Method to save, update Reader_Study object records and
                      to check if duplicate study__c value is selected.
        Parameters  : oReaderStudy Record
        Returns     : duplicate Exist  ? Error Message else Insert/Update Reader Study
    ******************************************************************************************
     public pagereference saveReaderStudy (){       
        pagereference pg = new pagereference('/apex/ReaderDetailsView?id='+readerID);
        //Condition to check if Study__c field is empty.
        if(oReaderStudy.study__C ==null){
            error = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Select study'));   
            return null;
        }
        set<Id>  readerMap = new set<Id>();
        readerMap.add(oReaderStudy.Reader__C);
        List<Reader_Study__c> readerStudyList = new List<Reader_Study__c>();
        readerStudyList.add(oReaderStudy);
        //Fetching result from checkDuplicateStudy method to check if duplicate study exists.
        Boolean duplicateflag = ReaderStudy_TriggerHandler.checkDuplicateStudy(readerMap , readerStudyList);
        if(!duplicateflag){
            try{
                upsert oReaderStudy;
            }
            catch (Exception Ex){                
            }
            error = false;
            pg = new pagereference('/apex/ReaderDetailsView?id='+readerID);
        }else{
            error = true;
            //Displaying error if duplicate Study__c value exists.
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, ' Duplicate Study.'));   
            return null;        
        }       
        pg.setredirect(true);
        return pg;
     }
     /******************************************************************************************
        Purpose     : Method to navigate back to the ReaderDetailsView page on cancel 
        Parameters  : readerID 
        Returns     : ReaderDetailsView page.
    ******************************************************************************************
     public pagereference cancelReaderStudy (){
        pagereference pg = new pagereference('/apex/ReaderDetailsView?id='+readerID);
        pg.setredirect(true);
        return pg;
     }  */ 
}