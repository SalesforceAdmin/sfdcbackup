global class ReaderManagementTestUtility {
  
  /*****************************************************************************************
        Purpose                 : To create Reader Contact set of Records 
        Cross Obejct            : Reader RecordType Id of Contact
    ******************************************************************************************
  global static List<Contact> createTestReaderContacts(){
        List<Contact> testReaderContacts = new List<Contact>(); 
        //Retrives Reader RecordtypeID of Contact Object
        Id ContactrecordTypeId =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.CONTACT_RECORD_TYPE_READER).getRecordTypeId();
        Contact testReaderContact1 = new Contact(FirstName = 'Reader1', LastName = 'Test1', CurrencyIsoCode = 'USD',  Country__c = 'United States',  recordTypeId= ContactrecordTypeId , Ext_ReaderID__c=1);
        Contact testReaderContact2 = new Contact(FirstName = 'Reader2', LastName = 'Test2', CurrencyIsoCode = 'USD',  Country__c = 'United States',  recordTypeId= ContactrecordTypeId);
        Contact testReaderContact3 = new Contact(FirstName = 'Reader3', LastName = 'Test3', CurrencyIsoCode = 'USD',  Country__c = 'United States',  recordTypeId= ContactrecordTypeId);
        Contact testReaderContact4 = new Contact(FirstName = 'Reader4', LastName = 'Test4', CurrencyIsoCode = 'USD',  Country__c = 'United States',  recordTypeId= ContactrecordTypeId);
        Contact testTriggerReader  = new Contact(FirstName = 'ContactTriggerTEST', LastName = 'DONOTTOUCHContact', CurrencyIsoCode = 'USD',  Country__c = 'United States',  recordTypeId= ContactrecordTypeId);
        testReaderContacts.add(testReaderContact1);
        testReaderContacts.add(testReaderContact2);
        testReaderContacts.add(testReaderContact3);
        testReaderContacts.add(testReaderContact4);
        Database.insert(testReaderContacts, false);
        system.assert(testReaderContacts.size()>0);
        Database.insert(testTriggerReader, false);//For ContactTriggerHandler
        //insert testTriggerReader;//For ContactTriggerHandler
        return testReaderContacts;
    }
    
    /*****************************************************************************************
        Purpose                 : To create Reader set of Records 
        Cross Obejct            : Reader Contact
    ******************************************************************************************
    global static List<Reader__C> createTestReader(){
      //Creates Reader Contact Record
      List<Contact> testReaderContacts = createTestReaderContacts();
      
      Map<String, Id> contactNmToIdMap = new Map<String, Id>();
        for(Contact c : testReaderContacts){
          //Mapping -  Contact's First name and Contact Id
            contactNmToIdMap.put(c.FirstName, c.Id);
        }
      List<Reader__C> testReaders = new List<Reader__C>();
      Reader__C testReader1 = new Reader__c(Contact__C = contactNmToIdMap.get('Reader1'), Active_Potential__c ='Active');
      Reader__C testReader2 = new Reader__c(Contact__C = contactNmToIdMap.get('Reader2'));
      Reader__C testReader3 = new Reader__c(Contact__C = contactNmToIdMap.get('Reader3'));
      Reader__C testReader4 = new Reader__c(Contact__C = contactNmToIdMap.get('Reader4'));
      testReaders.add(testReader1);
        testReaders.add(testReader2);
        testReaders.add(testReader3);
        testReaders.add(testReader4);
        Database.insert(testReaders, false);
        system.assert(testReaders.size()>0);
        return testReaders;
    }
    /*****************************************************************************************
        Purpose                 : To create Reader_Modality__C set of Records 
        Cross Obejct            : Reader 
    ******************************************************************************************
    global static List<Reader_Modality__C> createTestReaderModality(){
      //Creates Reader__C  Record
      List<Reader_Modality__C>  testReaderModality = new List<Reader_Modality__C> ();
      List<Reader__C> testReaders = [select id, name ,  Contact__C, Contact__r.firstname from Reader__C];
      Map<String, Id> rmodalityNmToIdMap = new Map<String, Id>();
        for(Reader__c s : testReaders){
          //Mapping - Reader's Contact FirstName and Reader Id
            rmodalityNmToIdMap.put(s.Contact__r.firstname, s.Id);
        }
        Reader_Modality__C testModality1a = new Reader_Modality__C(Reader__c = rmodalityNmToIdMap.get('Reader1'), Reader_Contract_Rate__c = 2000, Modality__C ='CT');
        Reader_Modality__C testModality1b = new Reader_Modality__C(Reader__c = rmodalityNmToIdMap.get('Reader1'), Reader_Contract_Rate__c = null, Modality__C ='DXA');
        Reader_Modality__C testModality1c = new Reader_Modality__C(Reader__c = rmodalityNmToIdMap.get('Reader1'), Reader_Contract_Rate__c = 0, Modality__C ='MRI');
        Reader_Modality__C testModality2  = new Reader_Modality__C(Reader__c = rmodalityNmToIdMap.get('Reader2'), Reader_Contract_Rate__c = 0, Modality__C ='MRI');
        Reader_Modality__C testModality3  = new Reader_Modality__C(Reader__c = rmodalityNmToIdMap.get('Reader3'), Reader_Contract_Rate__c = 0, Modality__C ='MRI');
        Reader_Modality__C testModality4  = new Reader_Modality__C(Reader__c = rmodalityNmToIdMap.get('Reader4'), Reader_Contract_Rate__c = 0, Modality__C ='MRI');
        testReaderModality.add(testModality1a);
        testReaderModality.add(testModality1b);
        testReaderModality.add(testModality1c);
        testReaderModality.add(testModality2);
        testReaderModality.add(testModality3);
        testReaderModality.add(testModality4);
        Database.insert(testReaderModality, false);
        system.assert(testReaderModality.size()>0);
      return testReaderModality;
    }
    
    /*****************************************************************************************
        Purpose                 : To create Reader_Study__C set of Records 
        Cross Obejct            : Reader  and Contact
    ******************************************************************************************
    global static List<Reader_Study__C> createTestReaderStudy(){
      //Creates Reader__C  Record
      List<Reader__C> testReaderstInser =  ReaderManagementTestUtility.createTestReader();
      List<Reader__C> testReaders = [select id, name ,  Contact__C, Contact__r.firstname from Reader__C];
      Map<String, Id> rmodalityNmToIdMap = new Map<String, Id>();
        for(Reader__c s : testReaders){
          //Mapping - Reader's Contact FirstName and Reader Id
            rmodalityNmToIdMap.put(s.Contact__r.firstname, s.Id);
        }
       // Create Sponsor Account and Study
        List<Study__c> testStudyList = UnitTest_DataFactory.createTestStudyList();
        Map<String, Id> studyNmToIdMap = new Map<String, Id>();
        for(Study__c s : testStudyList){
          //Mapping : Study Name and Study Id
            studyNmToIdMap.put(s.Name, s.Id);
        }
      List<Reader_study__C> testReaderstudy = new List<Reader_study__C>();
      Reader_study__C testReader1 = new Reader_study__C(Study__c = studyNmToIdMap.get('0001A'), reader__C = rmodalityNmToIdMap.get('Reader1'));
      testReaderstudy.add(testReader1);
        Database.insert(testReaderstudy, false);
        system.assert(testReaderstudy.size()>0);
        return testReaderstudy;
    }
     /*****************************************************************************************
        Purpose                 : To create Custom Setting records of Tab - Reader 
    ******************************************************************************************
    global static List<Tab_ID__c> createTestTab(){
       List<Tab_ID__c> oTabList = new List<Tab_ID__c>();
       Tab_ID__c otab = new Tab_ID__c();
       oTab.Id__c = 'test';
       oTab.Name = 'Reader';
       oTabList.add(oTab);
       insert oTabList;
       system.assert(oTabList.size()>0);
      return oTabList;
     }*/
}