@IsTest
public class RfpeditpageTest
{
    
public static testmethod void Saverfpdetails()
{
    
    Test.starttest();
    account a = new account(name='sampleaccount',accountstatus__c='Active',Accountnumber='A100');
    insert a;
    
    account acc = new account(name ='sampleacc');
    system.assertEquals(acc.name,'sampleacc');
    
    rpf__c r = new rpf__c(name='sample',accountname__c=a.id);
    insert r;
    
    Apexpages.StandardController sc = new Apexpages.StandardController(r);
    rfpeditclass rfp = new rfpeditclass(sc);
    System.assertNotEquals(null, rfp.saverfpdetails());
    rfp.populateaccountfields();
    Test.stoptest();
    
}


}