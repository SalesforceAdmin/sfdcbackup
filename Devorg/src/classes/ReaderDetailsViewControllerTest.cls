@IsTest(seeAllData=False)
private class ReaderDetailsViewControllerTest{
    /*****************************************************************************************
        Purpose                 : To Cover Methods in ReaderDetailsViewController class
        Cross Obejct            : Reader, Contact and Reader Study
    ******************************************************************************************
    public Static testmethod void ReaderDetailsViewMethod(){
        Test.StartTest();  
        ReaderManagementTestUtility.createTestTab(); 
        List<Reader_Study__C>testReaderstudy = ReaderManagementTestUtility.createTestReaderStudy();
        List<Reader_Study__C> rsList = [select id, study__C, study__r.name , study__r.Account__C, study__r.Account__r.Name,  reader__C, reader__r.name,
                                         reader__r.Contact__C, reader__r.Contact__r.Firstname from Reader_Study__C ];
        system.assert(rsList.size() >0 );
        Map<String, String> rmStudyNmToIdMap = new Map<String, String>();
        for(Reader_Study__C s : rsList){
            rmStudyNmToIdMap.put(s.reader__r.Contact__r.firstname, s.reader__c);
        }
        //Add Reader where Reader study is not empty
        Reader__c oReaderobj = [select id, Contact__C from Reader__C where  id = : rmStudyNmToIdMap.get('Reader1') limit 1];
        system.assert(oReaderobj !=null);
        ApexPages.currentPage().getParameters().put('id', oReaderobj.id);
        ApexPages.currentPage().getParameters().put('Conid',oReaderobj.Contact__C);
        ApexPages.StandardController stdController = new ApexPages.StandardController(oReaderobj);
        ReaderDetailsViewController controller = new ReaderDetailsViewController(stdController);
        controller.SaveReader();
        controller.CancelReader();
        controller.oReader.Contact__C = null;
        controller.SaveReader();
        controller.CancelReader();
        
        ReaderDetailsViewController controller1 = new ReaderDetailsViewController(stdController);
        controller1.oReader.Contact__C = oReaderobj.Contact__C;
        controller1.rid = null;
        controller1.DeleteReader();
        Test.stopTest();
    }*/
}