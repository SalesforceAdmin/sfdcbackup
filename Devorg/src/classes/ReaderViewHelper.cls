public class ReaderViewHelper {

    /******************************************************************************************
        Purpose     : To get ReaderQuery.
        Parameters  : readerquery,whereflag,actpotential,ptDrService.
        Return      : string readerquery
    ******************************************************************************************
    public static String getReaderQuery ( String readerQuery , Boolean whereflag ,String actPotential, String ptDrService, String aoSpecialisation,  String ModVal){
        
         List<Reader__C> oReaderQuery = new List<Reader__C>(); 
         String readerQuery1  =  null; 
         //Checking if variable actpotential is not empty, null and no value is selected.
         if(actPotential!=null && actPotential !='' && actPotential !='--None--'){
            String strEscactPotential =  String.escapeSingleQuotes(actPotential);          
            if(whereflag){
                readerQuery +=  ' and Active_Potential__c LIKE \'%'+strEscactPotential+'%\'';
            }else{
                 whereflag = true;
                 readerQuery +=  ' where Active_Potential__c LIKE \'%'+strEscactPotential+'%\'';
            }           
         }       
         //Checking if variable aoSpecialisation is not empty, null and no value is selected.
         if(aoSpecialisation!=null && aoSpecialisation !='' && aoSpecialisation !='--None--'){
            String strEscaoSpecialisation =  String.escapeSingleQuotes(aoSpecialisation); 
            if(whereflag){
                readerQuery +=  ' and Area_of_Specialization__c INCLUDES (\''+aoSpecialisation+'\')';
            }else{
                 whereflag = true;
                 readerQuery +=  ' where Area_of_Specialization__c INCLUDES (\''+aoSpecialisation+'\')';
            }           
         }   
         
          //Checking if variable ModVal is not empty, null and no value is selected.
         if(ModVal!=null && ModVal !='' && ModVal !='--None--'){
            String strEscModVal =  String.escapeSingleQuotes(ModVal); 
            if(whereflag){
                readerQuery +=  ' and Modality__c INCLUDES (\''+ModVal+'\')';
            }else{
                 whereflag = true;
                 readerQuery +=  ' where Modality__c INCLUDES (\''+ModVal+'\')';
            }           
         }   
         
          //Checking if variable ptDrService is not empty, null and no value is selected.
         if(ptDrService!=null && ptDrService !='' && ptDrService !='--None--'){
            String strEscptDrService =  String.escapeSingleQuotes(ptDrService); 
            if(whereflag){
                readerQuery +=  ' and Pass_Through_or_Direct_Service__c LIKE \'%'+strEscptDrService+'%\'';
            }else{
                 whereflag = true;
                 readerQuery +=  ' where Pass_Through_or_Direct_Service__c LIKE \'%'+strEscptDrService+'%\'';
            }           
         }    
         return   readerQuery ;
    }
     
    /******************************************************************************************
        Purpose     : Method to fetch data from Reader, ReaderStudy and Study objects.
        Parameters  : searchquery,accountname,studycode,actpotential,ptDrService.
        Return      : wrapperlist oReaderDetails
    ****************************************************************************************** 
    public static  List<ReaderViewWrapper.ReaderDetails> getReaderDetailsExport(Boolean searchQuery, String AccountName , String StudyCode, String actPotential, String ptDrService,  String aoSpecialisation,  String ModVal){
         List<ReaderViewWrapper.ReaderDetails> oReaderDetails = new  List<ReaderViewWrapper.ReaderDetails> ();
         List<Reader__C> oReaderQuery = new List<Reader__C>();        
         String readerQuery  =  null;       
         set<Id> studyId = new set<Id>();
         map<Id, Study__C> oMapStudy = new map<id, study__C>();
         set<String> readerid =new set<String>();
         Boolean accStd = false;  
         //Checking if variable searchquery is true.  
         if(searchQuery){ 
             readerQuery  =  ReadersManagementDAO.ReaderQuery(); 
             readerid =getReaderStudyDetail(AccountName,  StudyCode);
             //Checking if readerid is not empty.
             if(!readerid.isempty()){
                readerQuery += ' where id in : readerid';
                accStd = true;         
             }else{
                //Checking if accoutname,studycode is not empty and null.
                if((AccountName !=null && AccountName !='') ||(StudyCode!=null && StudyCode!='')){
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ' No results found'));  
                    return null;
                }
             }
             //Fetching readerquery, adding condition to readerquery.           
             readerQuery  =  getReaderQuery (readerQuery, accStd ,  actPotential, ptDrService,  aoSpecialisation,   ModVal);
             readerQuery += ' order by Name desc limit 900';
             oReaderQuery = database.query(readerQuery);
         }else{
            readerQuery  =  ReadersManagementDAO.ReaderQuery(); 
            readerQuery += ' order by Name desc limit 900  ';
            oReaderQuery = database.query(readerQuery);
         }      
         //Looping over Reader object to the list of study Id's.
         for(Reader__C r : oReaderQuery){
            for( Reader_Study__c rd : r.Reader_Study__r){
                studyId.add(rd.Study__C);
            }
         }
         //Checking if collection studyId is not empty.
         if(!studyId.isempty()){
            String StudyQuery  =  ReadersManagementDAO.StudyQuery();
            oMapStudy = new Map<id,Study__c>((List<Study__c>)Database.query(StudyQuery));
         }
         //Fetch the details from reader object
         for(Reader__C r : oReaderQuery){
            //Checking condition if Reader_study field is empty
            if(!r.Reader_Study__r.isempty()){
                for(Reader_Study__c rd : r.Reader_Study__r){
                    ReaderViewWrapper.ReaderDetails oor = new ReaderViewWrapper.ReaderDetails();
                    Study__c oStd = new Study__c();
                    oStd  = oMapStudy.get(rd.Study__C);
                    oor = AssignReadertoWrapper(r);
                    oor.studyCode = oStd.Name; 
                    oor.accountName = oStd.Account__r.Name; 
                    //Fetching result by passing accountname,studycode,oor,rd values to filteredReaderDetails method.
                    Boolean addvalue  = filteredReaderDetails( AccountName ,  StudyCode,  oor.studyCode,oor.accountName , rd);                   
                    //Checking if variable addvalue is true.
                    if(addvalue){
                        oReaderDetails.add(oor);
                    }
                }            
            }else{
                    //Adding record values to oReaderDetails wrapper list.
                    ReaderViewWrapper.ReaderDetails oor = new ReaderViewWrapper.ReaderDetails();
                    oor = AssignReadertoWrapper(r);
                    oReaderDetails.add(oor);
            }       
         }
         if(oReaderDetails ==null || oReaderDetails.isempty() ){        
         }     
         return oReaderDetails;
    }
    
    /******************************************************************************************
        Purpose     : Assigning Reader__c object field values to wrapper class fields.
        Parameters  : r - reader__c object.
        Return      : instance of wrapperlist oor
    ******************************************************************************************
    public static ReaderViewWrapper.ReaderDetails AssignReadertoWrapper( Reader__C r){
        ReaderViewWrapper.ReaderDetails oor = new ReaderViewWrapper.ReaderDetails();
        oor.readerId = r.Name;
        oor.readerName = r.Contact__r.FirstName +' '+r.Contact__r.LastName  ;  
        oor.firstName = r.Contact__r.FirstName;
        oor.lastName = r.Contact__r.LastName;
        oor.email = r.Contact__r.email; 
        oor.fax = r.Contact__r.fax; 
        oor.Title  = r.Contact__r.Title; 
        oor.StreetAddress1 = r.Contact__r.Street_Address_1__c ; 
        oor.City  = r.Contact__r.City__c;
        oor.StateorProvince = r.Contact__r.State__c ; 
        oor.ZipCode  = r.Contact__r.Zip_Postal_Code__c; 
        oor.Country = r.Contact__r.Country__c;  
        oor.StreetAddress2  = r.Street_Address_2__c; 
        oor.City2 = r.Contact__r.City_2__c ;  
        oor.StateorProvince2 = r.Contact__r.State_Province_2__c;
        oor.ZipCode2  = r.Contact__r.Zip_Postal_Code_2__c; 
        oor.Country2 = r.Contact__r.Country_2__c ;  
        oor.Phone  = r.Contact__r.Phone; 
        oor.mobile = r.Contact__r.MobilePhone;
        oor.otherEmail ='';
        oor.alternateEmail =r.Contact__r.Alternate_Email__c; 
        oor.homePhone  = r.Contact__r.homePhone; 
        oor.otherPhone  = r.Contact__r.otherPhone; 
        oor.PrimaryPhone = r.Phone_1__c ; 
        oor.SecondaryPhone = r.Phone_2__c ; 
        oor.OtherPhone=''; 
        oor.fax = r.Contact__r.fax;
        if(r.Contact__r.Ext_ReaderID__c !=null){
            oor.extReaderId = Integer.valueOf(r.Contact__r.Ext_ReaderID__c);
        }
        oor.PrimaryEmail = r.Email_1__c;  
        oor.AlternateEmail = r.Email_2__c ;
        oor.ReaderAffiliation = r.Reader_Affiliation__c;  
        oor.ReaderAffiliationGroup =r.Reader_Affiliation_Group__c;
        oor.AreaofSpecialization = r.Area_of_Specialization__c ;
        oor.GeneralExperience= r.General_Experience__c;
        oor.ActivePotential = r.Active_Potential__c;
        oor.BioclinicaShare=r.BioClinica_Share__c;
        oor.modality = r.modality__c;  
        oor.availability = r.Availability__c ;
        oor.VisitationHistory = r.Visitation_History__c;
        oor.MedicalLicenseExpirationDate = r.Medical_License_Expiration_Date__c;
        oor.Pass_Through_or_Direct_Service = r.Pass_Through_or_Direct_Service__c;
        oor.recordId = r.ID ;  
        oor.ITContactonSite = r.IT_Contact_on_Site__c;
        oor.ConnectionSpeedMbp = r.Connection_Speed_Mbp_s__c; 
        oor.ConnectionType = r.Connection_Type__c;     
        oor.studyCode = null; 
        oor.accountName = null; 
        oor.Asset = r.AssetID__c;
        oor.contactId = r.Contact__c;
        
        // Added new 4 Fields. Assigning reader__c object field values to variables of wrapper list isntance.
        oor.CalculatedRateperHour= r.Calculated_Rate_per_Hour__c;
        oor.ContractRateperHour= r.Contract_Rate_per_Hour__c;
        oor.ContractTPperHour= r.Contract_TP_per_Hour__c;
        oor.ProjectedTPReadperHour= r.Projected_TP_Read_per_Hour__c;     
        return oor;
    }
    
    /******************************************************************************************
        Purpose     : Filter reader's record based on Study code (or) Account Name.
        Parameters  : accountname,studycode,oor,rd
        Return      : boolean value addvalue.
    ******************************************************************************************
    public static Boolean filteredReaderDetails (String AccountName , String StudyCode, String oorStudyCode , String oorAccName, Reader_Study__c rd){
        
        Boolean addvalue = false;
        Boolean addAccount = false;
        Boolean addStudy = false;
        //Checking if accountname is not null and empty.
        if(AccountName !=null && AccountName !=''){
            if( oorAccName.toUpperCase().Contains(AccountName.trim().toUpperCase())){
                addAccount = true;
            }else{
                addAccount = false;
            }
        }else{addAccount = true;}   
        //Checking if studycode is not null and empty.
        if(StudyCode !=null && StudyCode !=''){
            if(oorStudyCode.toUpperCase().contains(studyCode.trim().toUpperCase())){
                addStudy = true;
            }else{
                addStudy = false;
            }
        }else{addStudy = true;}
        
        //Checking if accountname,studycode is not null and empty.
        if((AccountName ==null  || AccountName =='') && (StudyCode ==null || StudyCode =='')){
            addvalue = true;
        }else if(addStudy && addAccount){
            addvalue = true;
        }
        return addvalue ;
    }
    
    /******************************************************************************************
        Purpose     : Search the reader's record based on Study code (or) Account Name
        Parameters  : accountname,studycode
        Return      :  readerId .
    ******************************************************************************************
    public static Set<String> getReaderStudyDetail(string AccountName, String StudyCode){
        set<String> readerId = new set<String>();
         String ReaderStudyQuery = null;
         Boolean AccountFlag = false;
          List<Reader_Study__C> oReaderdetailQuery = new List<Reader_Study__C>();
         //Checking if accountname is not null and empty.
         if(AccountName!=null && AccountName !=''){
            String strEscAccount = String.escapeSingleQuotes(AccountName); 
            //Fetching readerstudyquery.
            ReaderStudyQuery = ReadersManagementDAO.ReaderStudyQuery();
            ReaderStudyQuery +=  ' where Study__r.Account__r.Name LIKE \'%'+strEscAccount+'%\' ';
            AccountFlag = true;
         }
         //Checking if studycode is not null and emtpy.
         if( StudyCode!=null && StudyCode !=''){
            String strEscStudy = String.escapeSingleQuotes(StudyCode); 
            if(AccountFlag){
                ReaderStudyQuery +=  ' and Study__r.Name LIKE \'%'+strEscStudy+'%\'';
            }else{
                //Fetching readerstudyquery.
                ReaderStudyQuery = ReadersManagementDAO.ReaderStudyQuery();
                //Adding condition to readerstutyquery.
                ReaderStudyQuery +=  ' where Study__r.Name LIKE \'%'+strEscStudy+'%\'';
            }
         }
         //Checking if ReaderStudyQuery is not null and not empty.
         if(ReaderStudyQuery!=null && ReaderStudyQuery !=''){
            oReaderdetailQuery = database.query(ReaderStudyQuery);
            //Checking if fetched query is not empty.
            if(!oReaderdetailQuery.isempty()){
                for(Reader_Study__C rd :oReaderdetailQuery){
                    readerId.add(rd.reader__C);
                }
            }
         }
        return readerId;
    }
     
    /******************************************************************************************
        Purpose     : Check for duplicate Reader record.
        Parameters  : oReader
        Return      : dupReader boolean variable.
    ******************************************************************************************
    public static Boolean checkDuplicateReader(Reader__c oReader){
        
        Boolean dupReader = false;
        //Checking if reader object is not null, readername is not null and not emtpy and readercontact is not null.
        if(oReader!=null  && oReader.Contact__C !=null){
            String cid = oReader.Contact__C;
            //Fetching readerquery.
            String readerQuery  =  ReadersManagementDAO.ReaderQuery(); 
            readerQuery += ' where Contact__C = : cid ';
            List<Reader__c> duplicateReader = database.query(readerQuery);
            //Checking if list is not empty.   
            if(!duplicateReader.isempty() ){
                if((oReader.NAme ==null || oReader.Name =='') && duplicateReader.size()>0){
                    dupReader = true;
                }else if(duplicateReader.size() == 1 &&  duplicateReader[0].Id == oReader.ID){
                    dupReader = false;
                }else{
                    dupReader = true;
                }
                
            }       
        }
        return dupReader;
    }
    
    /******************************************************************************************
        Purpose     : Assigning Reader__c field values to variables of wrapperclass.
        Parameters  : oReaderList,AccountName,StudyCode.
        Return      : oReaderDetails wrapperlist.
    ******************************************************************************************
    public static List<ReaderViewWrapper.ReaderDetails> AssignReaderDetailstoWrppaer( List<Reader__c> oReaderList, String AccountName,String StudyCode  ){
         //Fetch the details from reader object
         List<ReaderViewWrapper.ReaderDetails> oReaderDetails = new  List<ReaderViewWrapper.ReaderDetails> ();
          map<Id, Study__C> oMapStudy = new map<Id, Study__C>();
          set<Id> studyId = new set<Id>();
         //Looping over Reader object to the list of study Id's.
         for(Reader__C r : oReaderList){
            for( Reader_Study__c rd : r.Reader_Study__r){
                studyId.add(rd.Study__C);
            }
         }
         //Checking if studyId collection is not emtpy.
         if(!studyId.isempty()){
            String StudyQuery  =  ReadersManagementDAO.StudyQuery();
            oMapStudy = new Map<id,Study__c>((List<Study__c>)Database.query(StudyQuery));
         }
         //Looping over Reader object with list of reader object records.
         for(Reader__C r : oReaderList){
               ReaderViewWrapper.ReaderDetails oor = new ReaderViewWrapper.ReaderDetails();
               oor = AssignReadertoWrapper(r);
               oor.oStudydetails = new List<ReaderViewWrapper.Studydetails>();
            //Checking condition if Reader_study field is empty
            if(!r.Reader_Study__r.isempty()){
                //Looping over Reader_Study__c object
                Boolean addflag = false;
                for(Reader_Study__c rd : r.Reader_Study__r){
                    Study__c oStd = new Study__c();
                    oStd  = oMapStudy.get(rd.Study__C);
                    ReaderViewWrapper.Studydetails stdDetails = new ReaderViewWrapper.Studydetails();
                    stdDetails.studyCode = oStd.Name; 
                    stdDetails.accountName = oStd.Account__r.Name; 
                    stdDetails.StudyId = oStd.Id; 
                    stdDetails.AccountId = oStd.Account__c; 
                    //Fetching result from filteredReaderDetails method by passing accoutnname,studycode,oor,rd variables.
                    Boolean addvalue  = filteredReaderDetails( AccountName ,  StudyCode, stdDetails.studyCode,stdDetails.accountName, rd);
                    if(addvalue){
                        addflag = true;
                        oor.oStudydetails.add(stdDetails);                       
                    }
                } 
                if(addflag){
                        oor.StudyExist = true;
                        oReaderDetails.add(oor);
                 }           
            }else{                   
                    oor.StudyExist = false;
                    oReaderDetails.add(oor);
            }       
         }
         if(oReaderDetails ==null || oReaderDetails.isempty() ){                  
         }     
         return oReaderDetails;  
    }   
    
    public static void UpsertReaderModality(List<Reader_Modality__c> oReaderModality, Reader__c oReader){
        List<Reader_Modality__c> oReaderModalityUpsert  = new List<Reader_Modality__c>(); 
        List<Reader_Modality__c> oReaderModalityDelete  = new List<Reader_Modality__c>(); 
        for(Reader_Modality__c oRm : oReaderModality){
            if(oRm.Reader_Contract_Rate__c !=null || oRm.Sponsor_Contract_Reader_Rate__c !=null){
                oRm.Reader__c = oReader.ID;
                oReaderModalityUpsert.add(oRm);
            }else{
                oRm.Reader__c = oReader.ID;
                oRm.Reader_Contract_Rate__c =0;
                oRm.Sponsor_Contract_Reader_Rate__c =0;
                oReaderModalityUpsert.add(oRm);
            }
        }
        if(!oReaderModalityUpsert.isempty()){
            upsert oReaderModalityUpsert;
        }
        oReaderModalityDelete = ModalityViewList( oReader , true);
        if(!oReaderModalityDelete.isempty()){
            delete oReaderModalityDelete;
        }
        
     }
    
    public static List<Reader_Modality__c>  ModalityViewList(Reader__C oReader , Boolean deleteOperation){
        List<Reader_Modality__c> oReaderModality  = new List<Reader_Modality__c>();
        set<String> modalityExistList = new set<String>();
        List<String> modalityList = new List<String>();
        map<String, Reader_Modality__c> rmMap = new map<String, Reader_Modality__c>();
        if(oReader.Reader_Modality__r !=null){
            for(Reader_Modality__c rm : oReader.Reader_Modality__r){
                Reader_Modality__c r = new Reader_Modality__c();
                rmMap.put( rm.Modality__c, rm);
                modalityExistList.add(rm.Modality__c);
            }
        }    
        if(oReader.Modality__C!=null ){
            if(oReader.modality__c.contains(';')){
                modalityList = oReader.modality__c.split(';');
            }else{
                modalityList.add(oReader.modality__c);
            }
            if(deleteOperation){
                set<String> duvalue = new set<String>();
                duvalue = new set<String>(modalityList);
                for(String m : rmMap.keyset()){
                    if(!duvalue.contains(m)){
                        oReaderModality.add(rmMap.get(m));
                    }
                }
            }else{
                for(String m :modalityList){
                    if(!modalityExistList.contains(m)){
                        Reader_Modality__c rm = new Reader_Modality__c();
                        rm.modality__c = m;
                        rm.Reader_Contract_Rate__c = null;
                        rm.Sponsor_Contract_Reader_Rate__c = null;
                        oReaderModality.add(rm);
                    }else{
                        oReaderModality.add(rmMap.get(m));
                    }
                }
            }
            
        }else{
            if(deleteOperation){
                set<String> duvalue = new set<String>();
                duvalue = new set<String>(modalityList);
                for(String m : rmMap.keyset()){
                    if(!duvalue.contains(m)){
                        oReaderModality.add(rmMap.get(m));
                    }
                }
            }
        }
        return oReaderModality; 
        
     }
     
   /* public static List<ImgFacilityWrapper>  getImagingfacility(String readerId, String studyID){
         List<ImgFacilityWrapper> ifwList = new List<ImgFacilityWrapper>();
         String readerQuery  =  ReadersManagementDAO.ReaderQuery(); 
         readerQuery += ' where id = :readerId limit 1';
         List<Reader__c> oReaderList = database.query(readerQuery);
         List<String> modality = new List<String>();
         if(!oReaderList.isempty()){
            if(oReaderList[0].modality__c!=null){
                if(oReaderList[0].modality__c.contains(';')){
                    modality = oReaderList[0].modality__c.split(';');
                }else{
                    modality.add(oReaderList[0].modality__c);
                }
            }
             String ImagingFacilityQuery  =  ReadersManagementDAO.ImagingFacilityQuery(); 
             ImagingFacilityQuery += ' where Study__C = :studyID  and modality__C in : modality';
             List<Imaging_Facility__C > oifList = database.query(ImagingFacilityQuery);
             if(!oifList.isempty()){
                for(Imaging_Facility__C im : oifList){
                    ImgFacilityWrapper ifw = new ImgFacilityWrapper();
                    ifw.studyID = studyID;
                    ifw.readerID = readerID;
                    ifw.ImgID = im.id;
                    ifw.ImgModality = im.Modality__C;
                    ifw.ImgName = im.Name;
                    ifwList.add(ifw);
                }
             }
         }        
         return ifwList;
    }
    
    public class ImgFacilityWrapper{
        public string studyID {get;set;}
        public string readerID {get;set;}
        public string ImgID {get;set;}
        public string ImgName {get;set;}
        public string ImgModality {get;set;}
        
    }*/
}