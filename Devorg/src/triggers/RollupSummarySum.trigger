trigger RollupSummarySum on Opportunity (after insert,after update) 
{
    set<id> accountids = new set<id>();
    
    if(trigger.isinsert || trigger.isupdate)
    {
        for(opportunity opp: trigger.new)
        {
            accountids.add(opp.accountid);
        }
    
        map<id,double> AccountMap = new map<id,double>();
        for(aggregateresult q : [select accountid,sum(amount) from opportunity where accountid =: accountids group by accountid])
        {
            AccountMap.put((Id)q.get('accountid'),(Double)q.get('expr0'));
        }
    
        list<account> accountstoupdate = new list<account>();
        for(Account a : [Select Id, opportunitiesamount__c from Account where Id IN :AccountIds])
        {
            Double PaymentSum = AccountMap.get(a.id);
            a.opportunitiesamount__c = PaymentSum;
            AccountsToUpdate.add(a);
        }
        
        update AccountsToUpdate;  
    }     
}