trigger contactphone on Contact (after update) 
{
   list<account> ls_acc = new list<account>();
   
   // If a phone field is updated in contact, the corresponding account phone field has to be updated.
   if(trigger.isupdate)
       {
           for(contact con : trigger.new)
           {
              if(con.phone != trigger.oldmap.get(con.id).phone)
               
               {
                   account acc = new account();
                   acc.id = con.accountid;
                   acc.phone = con.phone;
                   ls_acc.add(acc);
               }
           }
       } 
   update ls_acc;
}