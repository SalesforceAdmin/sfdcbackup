trigger FieldUpdateScenarios on Account (before insert) 
{
    list<contact> con_list = new list<contact>();
    list<id> acc_list = new list<id>();
    map<id,string> acc_map =  new map<id,string>();
    list<contact> con_updatelist = new list<contact>();
    list<contact> con_insertlist = new list<contact>();
    set<id> acc_set = new set<id>();
    map<id,string> map_acc = new map<id,string>();
    
    /*if(trigger.isbefore && (trigger.isinsert || trigger.isupdate))
    {
       /* // To change field value if it is null.
        for(Account acc:trigger.new)
        {    
            if(acc.AccountStatus__c == null || acc.AccountStatus__c =='')
            {
                acc.AccountStatus__c = 'Active';
            }
        }      
    }*/
    
    /*if(trigger.isafter && (trigger.isinsert || trigger.isupdate))
    {   
         If an account is inserted, insert 10 contacts for that account.
        for(Account acc:trigger.new)
        {
            for(integer i=0; i<=10; i++)
            {
                contact con = new contact();
                con.accountid = acc.id;
                con.lastname  = acc.name + i;
                con_list.add(con);
            }          
        }
        
        insert con_list;
               
        // If a field in account is updated, the same field in contact which is related to account also needs to be updated.
        for(Account acc:trigger.new)
        {
            acc_list.add(acc.id);
            acc_map.put(acc.id,acc.Accountstatus__c);             
            con_list =[select id,name from contact where accountid =: acc_list];               
            for(contact con : con_list)
            {
                 con.Accountstatus__c = acc_map.get(acc.id);
                 con_updatelist.add(con);
            }    
         }                  
            if(con_updatelist.size() != null)
            {
                update con_updatelist;
            }       
       } */
       
       /*if(trigger.isafter || trigger.isupdate)
       {
           for(account acc : trigger.new)
           {
               acc_set.add(acc.id);
               acc_map.put(acc.id,acc.phone);
           }
           
           list<contact> con_querylist = [select id,accountid,phone from contact where accountid =: acc_set];
          for(contact con : con_querylist)
          {
              if(con_querylist.size() == 0)
              {
                      con.phone     = acc_map.get(con.accountid);
                      con.accountid = acc_map.get(con.phone);
                      con_insertlist.add(con); 
                      break;                          
              }
              else
              {
                  if(con.accountid!=acc_map.get(con.phone))
                  {
                      con.phone = acc_map.get(con.accountid);
                      con_updatelist.add(con);
                  }
              }
             
          }
      if(con_insertlist.size() > 0)
      {
          insert con_insertlist;
      }
      if(con_updatelist.size()>0)
      {
          update con_updatelist;
      }
      
      }*/
      //trigger updateContactOtherAddress on Account(after insert, after update) {  
    /*if (trigger.isUpdate) {
        //Identify Account Address Changes
        Set<Id> setAccountAddressChangedIds = new Set<Id>();

        for (Account oAccount : trigger.new) {
            Account oOldAccount = trigger.oldMap.get(oAccount.Id);

            boolean bIsChanged = (oAccount.BillingStreet != oOldAccount.BillingStreet || oAccount.BillingCity != oOldAccount.BillingCity);
            if (bIsChanged) {
                setAccountAddressChangedIds.add(oAccount.Id);
            }
        }

        //If any, get contacts associated with each account
        if (setAccountAddressChangedIds.isEmpty() == false) {
            List<Contact> listContacts = [SELECT Id, AccountId FROM Contact WHERE AccountId IN :setAccountAddressChangedIds];
            for (Contact oContact : listContacts) {
                //Get Account
                 account oAccount = trigger.newMap.get(oContact.AccountId);

                //Set Address
                oContact.OtherStreet = oAccount.BillingStreet;
                oContact.OtherCity = oAccount.BillingCity;
                oContact.OtherState = oAccount.BillingState;
                oContact.OtherPostalCode = oAccount.BillingPostalCode;
            }

            //If any, execute DML command to save contact addresses
            if (listContacts.isEmpty() == false) {
                update listContacts;
            }
        }
    }*/
    
   for(account a : trigger.new)
   {
       list<account> acclist = [select id from account where name =: a.name];
       if(acclist.size() > 0)
       {
           a.name.adderror('Name already exists');
       }
   }
}